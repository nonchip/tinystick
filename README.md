# TinyStick
Attiny85 based programmable usb stick thingy.

## Hardware

see the `hardware` folder for kicad files.

* the Attiny is powered at 5V, uses Zeners to clamp down the USB signal levels
* the USB signals are on pins `PB0` and `PB2`
* the `ISP` header contains 6pin isp signals and power
* the `PB` header contains port B (including the usb signals) in logical order (`PB0` is pin 1 and `PB5` is pin 6)

### BOM:
* `U1`: ATtiny85-20SU
* `R1`: 1K5 0603
* `R2`,`R3`: 68R 0603
* `C1`: 10uF tantalum EIA-3216
* `C2`: .1uF ceramic 0603
* `D1`, `D2`: 3V6 zener SOD-323

## Firmware

* symlink `firmware/mn-config` to `firmware/micronucleus/firmware/configuration/tinystick`
* build using `make PROGRAMMER="$PROGRAMMER" CONFIG=tinystick $VERB` where
  * `$PROGRAMMER` is the avrdude options, like e.g. `-c stk500v2 -P /dev/ttyUSB0` for the "mySmartUSB light" I got.
  * `$VERB` is one or multiple (in order) of:
    * `clean` should always be done before `all` to make sure there's no bytecode left
    * `all` builds the bootloader
    * `flash` uploads the bootloader
    * `fuse` sets the fuses
    * `disablereset` **WARNING!!!** prevents you from using the ISP header in the future, unless you re-enable it with the so called "HVSP" mode unsupported by most programmers. so only use this if you really need to use the reset pin (`PB5`) and are ok with doing a HVSP recovery should you accidentally brick the usb bootloader in the future
